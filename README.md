## Releasing with Kubernetes HELM ##

When releasing into Kubernetes then [Helm](https://helm.sh) is a must have tool.    
Let's make a demo of showing why.

## Goals of the demo ##

* Howto package an Spring Boot application as Kubernetes Helm chart
* Howto install an Spring Boot application as Kubernetes Helm chart
* Howto upgrade an Spring Boot application installed through Kubernetes Helm
* Howto rollback an Spring Boot application installed through Kubernetes Helm to previous version

## Prerequisites ##

* Kubernetes [minikube](https://kubernetes.io/docs/setup/minikube/) installation    
* Simple Spring MVC controller shipped with [Spring Boot application](https://spring.io/projects/spring-boot)    

Code:

		package com.example.kubernetes.demo.mvc;

		import org.springframework.web.bind.annotation.RequestMapping;
		import org.springframework.web.bind.annotation.RestController;

		/**
 		* Created by tomask79 on 20.06.18.
 		*/
		@RestController
		public class ControllerMVC {

    		@RequestMapping("/sayhello")
    		public String mvcTest() {
        		return "I'm saying hello to Kubernetes!";
    		}
		}
	
Dockerfile for building the Docker image:

		FROM openjdk:8-jre
		MAINTAINER Tomas Kloucek <tomas.kloucek@embedit.cz>

		ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/myservice/myservice.jar"]

		# Add the service itself
		ARG JAR_FILE
		ADD target/${JAR_FILE} /usr/share/myservice/myservice.jar

I like to build Docker images directly from maven so build is trigered by    
[Dockerfile maven plugin](https://github.com/spotify/dockerfile-maven) Hence in the pom.xml is added:    

			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>dockerfile-maven-plugin</artifactId>
				<version>1.4.7</version>
				<executions>
					<execution>
						<id>default</id>
						<goals>
							<goal>build</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<repository>test-mvc</repository>
					<tag>${project.version}</tag>
					<buildArgs>
						<JAR_FILE>${project.build.finalName}.jar</JAR_FILE>
					</buildArgs>
				</configuration>
			</plugin>

## Howto install Kubernetes Helm ##

Check [Helm installation guide](https://docs.helm.sh/using_helm/#installing-helm) and follow the steps according to your OS.    
In my case I've got MacOS so I did the following:    

* brew install kubernetes-helm
* helm init 

"Helm init" installs necessary Helm component called Tiller(Helm server)    
into kubernetes cluster returned by **kubectl config current-context**    
So you need to have your minikube started before that.

To verify that Tiller is ready just run:

	Macbooks-MacBook-Pro:charts tomask79$ kubectl -n kube-system get pods
	NAME                                    READY     STATUS    RESTARTS   AGE
	etcd-minikube                           1/1       Running   0          9m
	kube-addon-manager-minikube             1/1       Running   0          9m
	kube-apiserver-minikube                 1/1       Running   0          9m
	kube-controller-manager-minikube        1/1       Running   0          9m
	kube-dns-86f4d74b45-nvvw4               3/3       Running   0          10m
	kube-proxy-gd2m6                        1/1       Running   0          10m
	kube-scheduler-minikube                 1/1       Running   0          9m
	kubernetes-dashboard-5498ccf677-sbvvd   1/1       Running   0          10m
	storage-provisioner                     1/1       Running   0          10m
	tiller-deploy-6fd8d857bc-b4hpg          1/1       Running   0          15s

Okay, now you should be ready for Helming!

## Building the Docker images for Helming ##

First as usual switch your docker to be talking to docker daemon running    
inside of minikube:

	Macbooks-MacBook-Pro:spring-boot-kubernetes-deploy tomask79$ eval $(minikube docker-env)

Now build the application and docker image eventually:

	Macbooks-MacBook-Pro:spring-boot-kubernetes-deploy tomask79$ mvn clean install

Then for example change the SpringMVC controller at /sayhello address to be printing    
something else and run **mvn clean install again with different ${project.version}**    
in pom.xml. This will result into creating another docker image. In the end you should have:    

	Macbooks-MacBook-Pro:spring-boot-kubernetes-deploy tomask79$ docker images
	REPOSITORY                                 TAG                 IMAGE ID            CREATED             SIZE
	test-mvc                                   0.0.1-SNAPSHOT      8bb54cac5fdd        8 seconds ago       459MB
	test-mvc                                   0.0.2-SNAPSHOT      69498681609d        2 minutes ago       459MB
	openjdk                                    8-jre               60648c86cfe8        8 days ago          443MB
	gcr.io/kubernetes-helm/tiller              v2.11.0             ac5f7ee9ae7e        4 weeks ago         71.8MB

## Creating and packaging HELM chart ##

To create an empty [Helm chart](https://docs.helm.sh/developing_charts/) simply just run:

	Macbooks-MacBook-Pro:workspace tomask79$ helm create testmvc-chart
	Creating testmvc-chart

After that you'll get [chart templates](https://docs.helm.sh/chart_template_guide/#charts) files.    
If you open deployment.yaml or service.yaml you will see three types of values:

	{{ template �chart.fullname� . }} # values from Chart.yaml 
	{{ .Release.Name }} # built in Release object
	{{ .Values.replicaCount }} # value from values.yaml

This means:

* You can parametrize your Kubernetes manifests
* HELM will install them all at once!
* HELM is able to drop them all at once!
* HELM is able to upgrade and rollback them all at once!

These are the main reasons why you should embrace HELM.    
First edit your **values.yaml** and internalPort, because we're going    
to use NodePort service.

	replicaCount: 1
	image:
  		repository: test-mvc
  		tag: 0.0.1-SNAPSHOT
  		pullPolicy: IfNotPresent
	service:
  		name: testmvc
  		type: NodePort
  		externalPort: 8081
  		internalPort: 8081
	ingress:
  		enabled: false
  		# Used to create an Ingress record.
  		hosts:
    		- chart-example.local
  		annotations:
    		# kubernetes.io/ingress.class: nginx
    		# kubernetes.io/tls-acme: "true"
  		tls:
    		# Secrets must be manually created in the namespace.
    		# - secretName: chart-example-tls
    		#   hosts:
    		#     - chart-example.local
	resources: {}

Then **deployment.yaml** chart template, image details are taken from values.yaml:

	apiVersion: extensions/v1beta1
	kind: Deployment
	metadata:
  		name: {{ .Chart.Name }}
	spec:
  		replicas: {{ .Values.replicaCount }}
  	template: 
    	metadata:
      		labels:
        	app: {{ .Chart.Name }}
    	spec:
      	containers:
      	- name: {{ .Chart.Name }}
        	image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
        	imagePullPolicy: {{ .Values.image.pullPolicy }}
        	ports:
        	- containerPort: {{ .Values.service.internalPort }}

**service.yaml** will contain NodePort settings, check service.type in values.yaml:

	apiVersion: v1
	kind: Service
	metadata:
  		name: {{ .Chart.Name }}
  	labels:
    	name: {{ .Chart.Name }}
	spec:
  		type: {{ .Values.service.type }}
  	ports:
      - port: {{ .Values.service.externalPort}}
        targetPort: {{ .Values.service.internalPort}}
  	selector:
    	app: {{ .Chart.Name }}

Templates are ready, so let's **create and package a chart for the    
first docker image with first version of my demo app**

	Macbooks-MacBook-Pro:workspace tomask79$ helm package ./testmvc-chart --debug
	Successfully packaged chart and saved it to: /Users/tomask79/workspace/testmvc-chart-0.1.0.tgz
	[debug] Successfully saved /Users/tomask79/workspace/testmvc-chart-0.1.0.tgz to /Users/tomask79/.helm/repository/local

To prepare a chart for upgrade let's modify **Chart.yaml** and shift up the version:

	apiVersion: v1
	appVersion: "1.0"
	description: A Helm chart for Kubernetes
	name: testmvc-chart
	version: 0.2.0
	
and upgrade also image version in **values.yaml**:

	image:
  		repository: test-mvc
  		tag: 0.0.2-SNAPSHOT
  		pullPolicy: IfNotPresent

Now let's build a chart again but this time for the new version of our application:

	Macbooks-MacBook-Pro:workspace tomask79$ helm package ./testmvc-chart --debug
	Successfully packaged chart and saved it to: /Users/tomask79/workspace/testmvc-chart-0.2.0.tgz
	[debug] Successfully saved /Users/tomask79/workspace/testmvc-chart-0.2.0.tgz to /Users/tomask79/.helm/repository/local

I attached both charts to this repo, but I wanted to show you how I built them.

## Installing a HELM chart ##

Okay, application is ready in two HELM charts pointing to different docker images.

	Macbooks-MacBook-Pro:workspace tomask79$ ls -l | grep ".gz"
	-rw-r--r--  1 tomask79  staff  2268 Oct 25 22:06 testmvc-chart-0.1.0.tgz
	-rw-r--r--  1 tomask79  staff  2268 Oct 25 22:11 testmvc-chart-0.2.0.tgz

Let's install the first one:

	Macbooks-MacBook-Pro:workspace tomask79$ helm install -n testmv-chart testmvc-chart-0.1.0.tgz 
	NAME:   testmv-chart
		LAST DEPLOYED: Thu Oct 25 22:19:53 2018
		NAMESPACE: default
		STATUS: DEPLOYED

	RESOURCES:
	==> v1/Service
	NAME           AGE
	testmvc-chart  0s

	==> v1beta1/Deployment
	testmvc-chart  0s

	==> v1/Pod(related)

	NAME                            READY  STATUS             RESTARTS  AGE
	testmvc-chart-595dcdd84b-2mm4t  0/1    ContainerCreating  0         0s


	NOTES:
	1. Get the application URL by running these commands:
 	 export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services testmv-chart-testmvc-chart)
  	 export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  	 echo http://$NODE_IP:$NODE_PORT

After that you can verify that app POD is running:

	Macbooks-MacBook-Pro:workspace tomask79$ kubectl get pods
	NAME                             READY     STATUS    RESTARTS   AGE
	testmvc-chart-595dcdd84b-2mm4t   1/1       Running   0          2m

and HELM is having your chart in the installation list:

	Macbooks-MacBook-Pro:workspace tomask79$ helm list
	NAME        	REVISION	UPDATED                 	STATUS  	CHART              	APP VERSION	NAMESPACE
	testmv-chart	1       	Thu Oct 25 22:19:53 2018	DEPLOYED	testmvc-chart-0.1.0	1.0        	default  

So why don't we test the app that it works:

	Macbooks-MacBook-Pro:workspace tomask79$ kubectl get services
	NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
	kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP          1d
	testmvc-chart   NodePort    10.100.60.180   <none>        8081:31804/TCP   4m
	Macbooks-MacBook-Pro:workspace tomask79$ 
	Macbooks-MacBook-Pro:workspace tomask79$ minikube service testmvc-chart --url
	http://192.168.99.100:31804
	Macbooks-MacBook-Pro:workspace tomask79$ curl http://192.168.99.100:31804/sayhello
	I'm saying hello to Kubernetes!

Again notice, we installed whole K8s pack with just one helm install!

## Upgrading HELM chart to new version ##

We have our Spring Boot app also in the version 2    
packed in the **testmvc-chart-0.2.0.tgz** so let's upgrade existing HELM chart:

	Macbooks-MacBook-Pro:workspace tomask79$ helm upgrade testmv-chart testmvc-chart-0.2.0.tgz
	Release "testmv-chart" has been upgraded. Happy Helming!
	LAST DEPLOYED: Thu Oct 25 22:40:36 2018
	NAMESPACE: default
	STATUS: DEPLOYED

	RESOURCES:
	==> v1/Pod(related)
	NAME                            READY  STATUS       RESTARTS  AGE
	testmvc-chart-595dcdd84b-8mbpm  1/1    Terminating  0         16s
	testmvc-chart-74d945c54f-7l8rm  0/1    Pending      0         0s

	==> v1/Service

	NAME           AGE
	testmvc-chart  20m

	==> v1beta1/Deployment
	testmvc-chart  20m


	NOTES:
	1. Get the application URL by running these commands:
  		export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services testmv-chart-testmvc-chart)
  		export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
  		echo http://$NODE_IP:$NODE_PORT

Okay, let's check that the upgraded PODS are running and test the app:

	Macbooks-MacBook-Pro:workspace tomask79$ kubectl get pods
	NAME                             READY     STATUS    RESTARTS   AGE
	testmvc-chart-74d945c54f-7l8rm   1/1       Running   0          2m
	Macbooks-MacBook-Pro:workspace tomask79$ minikube service testmvc-chart --url
	http://192.168.99.100:31804
	Macbooks-MacBook-Pro:workspace tomask79$ curl http://192.168.99.100:31804/sayhello
	I'm saying hello to Kubernetes in the V2!

## Rollback of the chart to previous version ##

[Rollback to previous version](https://stackoverflow.com/questions/51894307/helm-rollback-to-previous-release) in K8s with HELM is very easy:

	Macbooks-MacBook-Pro:workspace tomask79$ helm rollback testmv-chart 0
	Rollback was a success! Happy Helming!
	Macbooks-MacBook-Pro:workspace tomask79$ helm list
	NAME        	REVISION	UPDATED                 	STATUS  	CHART              	APP VERSION	NAMESPACE
	testmv-chart	5       	Thu Oct 25 22:55:28 2018	DEPLOYED	testmvc-chart-0.1.0	1.0        	default  

Let's check that we're really back to version one:

	Macbooks-MacBook-Pro:workspace tomask79$ minikube service testmvc-chart --url
	http://192.168.99.100:31804
	Macbooks-MacBook-Pro:workspace tomask79$ curl http://192.168.99.100:31804/sayhello
	I'm saying hello to Kubernetes!

Happy Helming! :)

regards

Tomas